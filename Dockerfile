# Use the LinuxServer.io guacd base image
FROM lscr.io/linuxserver/guacd:latest

# Expose port
EXPOSE 4822

# Set the entrypoint command
ENTRYPOINT ["guacd"]
